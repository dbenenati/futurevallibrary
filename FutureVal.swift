import Foundation

public class FutureValue
{
    var principalString: String
    var interestString: String
    var periodString: String
    
    public init(interest: String, principal: String, period: String)
    {
        interestString = interest
        principalString = principal
        periodString = period
    }
    
    public func calcFutureValue()->String
    {
        let periodTemp = Double(periodString)
        let period = periodTemp! / 12
        let interest = Double(interestString)
        let principal = Double(principalString)
        
        let fValue = principal! * (pow((1 + interest!), period))
        let futureValueString = String("\((round(100 * fValue) / 100))")
        return futureValueString!
        
    }
    
}
